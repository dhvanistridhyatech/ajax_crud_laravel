<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('dashboard', function () {
    return view('Dashboard');
});

Route::get('list', function () {
    return view('list');
});



Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/Addusers', 'MainController@viewAddUserForm')->name('adduser');
/*Route::get('/Addusers','MainController@addUserData');
*/Route::post('/addUserData','MainController@addUserData');
Route::post('/viewDashboard','MainController@viewDashboard');
Route::get('/show','MainController@viewDashboard');
Route::get('/deleteUser/{id}','MainController@deleteUserData');
Route::get('editblade','MainController@deleteUserData');

Route::get('/updateUser/{id}','MainController@updateUserData');
Route::post('/editUser','MainController@editUserData');
Route::get('/', 'Auth\LoginController@login');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::resource('ajax-crud', 'AjaxCrudController');

Route::post('ajax-crud/update', 'AjaxCrudController@update')->name('ajax-crud.update');

Route::get('ajax-crud/destroy/{id}', 'AjaxCrudController@destroy');
/*Route::get('ajax-crud', 'AjaxCrudController@show');*/

Route::get('/getUsers', 'PagesController@getUsers');
Route::get('/addUser', 'PagesController@addUser');
Route::get('/updateUser', 'PagesController@updateUser');
Route::get('/deleteUser/{id}', 'PagesController@deleteUser');
Route::resource('crud','CrudsController');
/*Route::get('/calldashboardajax','MainController@callDashboardajax');
Route::get('/viewdashboardajax','MainController@viewDashboardajax');*/
Route::resource('ajax-posts', 'ajaxcrud\AjaxPostController');
//Route::get('preview-image-upload', 'AjaxPostController@index');
//Route::post('preview-image-upload', 'AjaxPostController@store');
