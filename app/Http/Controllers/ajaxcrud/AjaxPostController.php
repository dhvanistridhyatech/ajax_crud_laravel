<?php

namespace App\Http\Controllers\ajaxcrud;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use Redirect,Response;

class AjaxPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['posts'] = Post::orderBy('id','desc')->paginate(8);
   
        return view('ajaxcrud.index',$data);
    }
     

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
/*
        request()->validate([
            'profile_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
       ]);
       if ($files = $request->file('profile_image')) {
        // Define upload path
           $destinationPath = public_path('/profile_images/'); // upload path
        // Upload Orginal Image           
           $profileImage = date('YmdHis') . "." . $files->getClientOriginalExtension();
           $files->move($destinationPath, $profileImage);

           $insert['image'] = "$profileImage";
           print_r($insert['image']);
        // Save In Database
            $post= new Posts();
            $post->photo_name="$profileImage";
            $post->save();
        }
        return back()->with('success', 'Image Upload successfully');
 */
        $postID = $request->post_id;
        $post   =   Post::updateOrCreate(['id' => $postID],
                    ['first_name' => $request->first_name, 'last_name' => $request->last_name,'email'=> $request->email,'pwd'=> $request->pwd]);
    
        return Response::json($post);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $where = array('id' => $id);
        $post  = Post::where($where)->first();
 
        return Response::json($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $post = Post::where('id',$id)->delete();
   
        return Response::json($post);
    }
}