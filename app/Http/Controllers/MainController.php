<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Auth;

class MainController extends Controller
{


public function index()
    {   
        $users = User::paginate(3);
        return view('users',$users);
    }
   
    public function viewAddUserForm()
    {
     return view('admin.AddUsers');

    }
    
    public function addUserData(Request $req)
    {
             $validator = Validator::make($req->all(), [
                'email' => 'required|max:255',
                'firstName' => 'required',
                'lastName' => 'required',
                'gender'=>'required',
                'profileImage'=>'required',
                'password'=>'required',
              ]);

              if ($validator->fails()) {
                  return redirect('/Addusers')
                              ->withErrors($validator)
                              ->withInput();
              } 
               $user = new User;
               $user->first_name = $req->firstName;
               $user->last_name = $req->lastName;
               $user->gender = $req->gender;
               $user->email= $req->email;
              // $user->profileImage= $req->profileImage;
               $t = time();
               $img = $t.'_'.$req->file('profileImage')->getClientOriginalName();
               $user->profileImage = $img;
               $user->status= $req->status;
               $user->password = \Hash::make($req->password);

               $user->save();
                

               //$req->file('profileImage')->move('upload/',$img);
              return redirect('/show');
           
    }
   public function viewDashboard()
      {
        $users = new User();
        $user = $users::paginate(3); 
        return view('Dashboard',['data'=>$user]);
      }

   public function deleteUserData(Request $req)
      {
        DB::table('users')->where('id', '=',$req->id)->delete();
        return redirect('/show')->with('success','successfully deleted.');
      }
     
   public function editUserData(Request $req)
    {


        $user = new User;
        $user->image= $req->profileImage;
        $t = time();
        $img = $t.'_'.$req->file('profileImage')->getClientOriginalName();
        $user_info =
                 array ( 

                    'first_name' => $req->firstName,
                    'last_name' => $req->lastName,
                    'gender' => $req->gender,
                    'email'=> $req->email,
                    'status' => $req->status,
                    'profileImage'=>$img,
                    'password' => $req->password

                 );
                  DB::table('users')->where('id', $req->id)->update($user_info);
                  $req->file('profileImage')->move('upload/',$img);
                 return redirect('/show');
    }

     public function updateUserData(Request $req)
      {


       $record = User::find($req->id);
       return view('editUser',['data'=>$record]);
      }

    
}
?>

