@extends('admin.layout.master')
@section('section')

<center>
 @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
      <h2> Registration </h2>
   {{ Form::open(array('url' => url('/addUserData'), 'files' => true, 'class'=>'reg-form')) }}
   
   <table width="50%" border="1px solid gray">
    <tbody style="background-color: lightblue;">
<tr>
      <td class="py-4">{!! Form::label('Role', '' , array('class'=>'form-lable')) !!}</td>
      <td class="py-4">{!! Form::select('role',array('Admin' => 'Admin', 'User' => 'User')) !!}</td>
     </tr>
     <tr>
      <td class="py-4">{!! Form::label('Fist Name', '' , array('class'=>'form-lable')) !!}</td>
      <td class="py-4">{!! Form::text('firstName', '', array('class' => 'form-control bg-white')) !!}</td>
     </tr>
     <tr>
      <td class="py-4">{!! Form::label('Last Name', '' , array('class'=>'form-lable')) !!}</td>
      <td class="py-4">{!! Form::text('lastName', '', array('class' => 'form-control bg-white')) !!}</td>
     </tr>

     <tr>
      <td class="py-4">{!! Form::label('Select gender', '' , array('class'=>'form-lable')) !!}</td>
      <td class="py-4">{!! Form::radio('gender', 'male') !!} male &nbsp;  {!! Form::radio('gender', 'male') !!}  female</td>
     </tr>
     <tr>
      <td class="py-4">{!! Form::label('Email', '' , array('class'=>'form-lable')) !!}</td>
      <td  class="py-4">{!! Form::email('email', '',array('class' => 'form-control bg-white')) !!}</td>
     </tr>

     <tr>
      <td class="py-4">{!! Form::label('Profile Image', '' , array('class'=>'form-lable')) !!}</td>
      <td class="py-4">{!! Form::file('profileImage') !!}</td>
     </tr>
      <tr>
      <td class="py-4">{!! Form::label('Status', '' , array('class'=>'form-lable')) !!}</td>
      <td class="py-4">{!! Form::select('status',array('Active' => 'Active', 'Deactive' => 'Deactive')) !!}</td>
     </tr>
     <tr>
      <td class="py-4">{!! Form::label('Password', '' , array('class'=>'form-lable')) !!}</td>
      <td class="py-4">{!! Form::password('password'); !!}</td>
     </tr>
    

    </tbody>
   </table>
    <tr><td>{!! Form::submit('submit',array('class'=>'btn btn-primary')); !!}</td></tr>
   </center>     

    {!! Form::close() !!}
@endsection