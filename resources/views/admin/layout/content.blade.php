<!DOCTYPE html>
<html lang="en">
@include('admin.layout.head')
<body class="">
<div class="wrapper ">
<div class="sidebar" data-color="orange">
<div class="logo">
  <a href="http://www.creative-tim.com" class="simple-text logo-mini">
  </a>
  <a href="http://www.creative-tim.com" class="simple-text logo-normal">
  Dhvani Shah
  </a>
</div>
<div class="sidebar-wrapper" id="sidebar-wrapper">
  <ul class="nav">
    <li>
    <a href="ajax-crud">
    <i class="now-ui-icons education_atom"></i>
    <p>Add Users</p>
    </a>
    </li>
  </ul>
</div>
</div>
<div class="main-panel" id="main-panel">
<nav class="navbar navbar-expand-lg navbar-transparent  bg-primary  navbar-absolute">
 <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-toggle">
              <button type="button" class="navbar-toggler">
                <span class="navbar-toggler-bar bar1"></span>
                <span class="navbar-toggler-bar bar2"></span>
                <span class="navbar-toggler-bar bar3"></span>
              </button>
            </div>
            <a class="navbar-brand" href="#pablo"></a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
            <span class="navbar-toggler-bar navbar-kebab"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navigation">
           
           
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                </div>
              </li>
             
            </ul>
          </div>
       
      </nav>
<div class="panel-header panel-header-lg"> </div>
@yield('section')
<script src="assets/js/core/jquery.min.js"></script>
<script src="assets/js/core/popper.min.js"></script>
<script src="assets/js/core/bootstrap.min.js"></script>
<script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
<script src="assets/js/plugins/chartjs.min.js"></script>
<script src="assets/js/plugins/bootstrap-notify.js"></script>
<script src="assets/js/now-ui-dashboard.min.js?v=1.5.0" type="text/javascript"></script>
<script src="assets/demo/demo.js"></script>
<script>
$(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      demo.initDashboardPageCharts();

});
</script>
</body>

</html>