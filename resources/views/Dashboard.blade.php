@extends('admin.layout.master')
@section('section')
   
<br><br>
      <div class="content">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title"> <center>View All Users</center></h4>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table" border="1px solid black">
                    <thead class=" text-primary">
                     <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                        Id
                      </th>
                      <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                        First Name
                      </th>
                       <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                        Last Name
                      </th>
                       <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                        Gender
                      </th>
                       <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                        Email
                      </th>
                       <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                        Image
                      </th>
                      <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                        Password
                      </th>
                       <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                        Edit
                      </th>
                       <th bgcolor="#28a74569" style="color: white;border: 1px solid black">
                       Delete
                      </th>
                    </thead>
                    <tbody style="background-color: #ffb6c178;">

                     @foreach($data as $value) 
                      <tr style="border: 1px solid black">
                        <td style="border: 1px solid black">
                         {{ $value->id }}
                        </td>
                        <td style="border: 1px solid black">
                          {{ $value->first_name }}
                        </td>
                        <td style="border: 1px solid black">
                          {{ $value->last_name }}
                        </td>
                         <td style="border: 1px solid black">
                          {{ $value->gender }}
                        </td >
                        <td style="border: 1px solid black">
                          {{ $value->email }}
                        </td>
                         <td style="border: 1px solid black">
                          <img src="{{asset('upload/'.$value->profileImage) }}">
                        </td>
                        <td style="border: 1px solid black">
                          {{ $value->password }}
                        </td>
                        <td style="border: 1px solid black">

                       <a href="/updateUser/{{ $value->id }}">    <button type="button" class="btn btn-info">Edit</button></a>

                        </td>
                        <td style="border: 1px solid black">
                       <a href="/deleteUser/{{ $value->id}}">   <button type="button" class="btn btn-warning">Delete</button></a>
                        </td>

                      </tr>
                      @endforeach

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

    
  @endsection