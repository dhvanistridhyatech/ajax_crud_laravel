<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
			DB::table('users')->insert([
            'email' => "admin@gmail.com",
            'password' =>Hash::make('password'),
            'role' => "Admin",
            'first_name' => "admin",
            'last_name' => "admin",
            'gender' => "female",
             'status' => "Active",

        ]);
    }
}
